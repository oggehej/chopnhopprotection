package com.creeperevents.oggehej;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.pauldavdesign.mineauz.minigames.Minigames;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class ChopNHopProtection extends JavaPlugin implements Listener
{
	WorldGuardPlugin wg = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
	Minigames mini = (Minigames) getServer().getPluginManager().getPlugin("Minigames");

	public void onEnable()
	{
		if(wg == null || mini == null)
		{
			getServer().getLogger().severe("["+getName()+"] Missing dependencies. Shutting down!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		this.getServer().getPluginManager().registerEvents(this, this);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		RegionManager rm = wg.getRegionManager(event.getBlock().getWorld());
		if(!mini.getPlayerData().getMinigamePlayer(event.getPlayer()).isInMinigame() && rm.hasRegion(getConfig().getString("RegionName")))
		{
			if(rm.getRegion(getConfig().getString("RegionName")).contains(event.getBlock().getX(), event.getBlock().getX(), event.getBlock().getZ()))
			{
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.RED + "Why not just accept that you didn't win and stop breaking stuff?");
			}
		}
	}
}
